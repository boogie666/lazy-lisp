(ns lazy-lisp.syntax
  (:require [clojure.spec.alpha :as s]))

(s/def :lisp/symbol symbol?)
(s/def :lisp/number number?)
(s/def :lisp/string string?)
(s/def :lisp/char char?)
(s/def :lisp/boolean boolean?)
(s/def :lisp/keyword keyword?)

(s/def :lisp/if-expr
  (s/and list?
         (s/cat :if #{'if}
                :condition :lisp/expr
                :consequent :lisp/expr
                :alternative :lisp/expr)))

(s/def :lisp/when-expr
  (s/and list?
         (s/cat :if #{'when}
                :condition :lisp/expr
                :body (s/* :lisp/expr))))

(s/def :lisp/fn-args
  (s/coll-of :lisp/symbol :kind vector?))

(s/def :lisp/fn-body
  (s/* :lisp/expr))

(s/def :lisp/fn-expr
  (s/and list?
         (s/cat :fn #{'fn 'λ}
                :name (s/? :lisp/symbol)
                :doc-string (s/? :lisp/string)
                :args :lisp/fn-args
                :body :lisp/expr)))

(s/def :lisp/apply-expr
  (s/and list?
         (s/* :lisp/expr)))

(s/def :lisp/quote-expr
  (s/and list?
         (s/cat :quote #{'quote}
                :body :lisp/expr)))

(s/def :lisp/do-expr
  (s/and list?
         (s/cat :do #{'do}
                :body (s/* :lisp/expr))))

(s/def :lisp/cons
  (s/and list?
         (s/cat :cons #{'cons}
                :a :lisp/expr
                :b :lisp/expr)))

(s/def :lisp/let-bindings
  (s/and vector?
         (s/*
          (s/cat :symbol :lisp/symbol
                 :expr :lisp/expr))))

(s/def :lisp/let
  (s/and list?
         (s/cat :let #{'let}
                :bindings :lisp/let-bindings
                :body (s/* :lisp/expr))))

(s/def :lisp/def
  (s/and list?
         (s/cat :def #{'def}
                :name :lisp/symbol
                :value :lisp/expr)))

(s/def :lisp/defn
  (s/and list?
         (s/cat :def #{'defn}
                :doc-string (s/? :lisp/string)
                :name :lisp/symbol
                :args :lisp/fn-args
                :body (s/* :lisp/expr))))



(s/def :lisp/expr
  (s/or :number :lisp/number
        :string :lisp/string
        :boolean :lisp/boolean
        :char :lisp/char
        :nil nil?
        :keyword :lisp/keyword
        :symbol :lisp/symbol
        :quote :lisp/quote-expr
        :if :lisp/if-expr
        :when :lisp/when-expr
        :do :lisp/do-expr
        :fn :lisp/fn-expr
        :let :lisp/let
        :def :lisp/def
        :defn :lisp/defn
        :list :lisp/apply-expr))


(defn parse [program]
  (s/conform :lisp/expr program))

(defn invalid? [ast]
  (= ast ::s/invalid))

(defn explain [ast]
  (s/explain-data :lisp/expr ast))
