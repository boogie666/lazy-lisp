(ns lazy-lisp.core
  (:gen-class)
  (:require [lazy-lisp.syntax :as s]
            [lazy-lisp.eval :as e]
            [clojure.string :as str]))


(defn lazy-lisp-load-file [ctx path]
  (let [path (e/lisp-eval path ctx)
        source (clojure.edn/read-string (str "(do " (slurp path) ")"))
        ast (s/parse source)]
    (if (s/invalid? ast)
      (throw (ex-info "Source is invalid." (s/explain ast)))
      (e/lisp-eval ast ctx))))


(def root-scope
  (atom {'cons [:core e/cons]
         'load-file [:core lazy-lisp-load-file]
         'print [:primitive println]
         'head [:primitive e/head]
         'tail [:primitive e/tail]
         'mod [:primitive mod]
         '< [:primitive <]
         '> [:primitive >]
         '= [:primitive =]
         '-   [:primitive -]
         '+   [:primitive +]
         '*   [:primitive *]
         '/   [:primitive /]}))



(defn -main [& args]
  (let [file [:string (first args)]]
    (lazy-lisp-load-file root-scope file)))




(comment
  (-main "./resources/multi_args.lclj"))
