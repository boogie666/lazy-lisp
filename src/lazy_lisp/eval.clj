(ns lazy-lisp.eval
  (:refer-clojure :exclude [fn? cons])
  (:require [clojure.string :as str]))


(declare force-it)
(declare delay-it)

(defprotocol ICons
  (head [_])
  (tail [_]))

(defrecord Cons [h t]
  ICons
  (head [_] h)
  (tail [_] t))

(defn cons [ctx a b]
  (let [new-ctx (atom @ctx)]
    (->Cons (delay-it a new-ctx) (delay-it b new-ctx))))

(defmethod print-method Cons [o w]
  (let [first-10 (loop [x o
                        count 0
                        acc []]
                   (if (and (< count 5) x)
                     (recur (force-it (tail x))
                            (inc count)
                            (conj acc (force-it (head x))))
                     acc))]
    (if (< (count first-10) 5)
      (print-simple (str "(" (str/join " " first-10) ")")
                    w)
      (print-simple (str "(" (str/join " " first-10) " ...)")
                    w))))



(defmulti lisp-eval (fn [[type _] ctx]
                      type))

(defmethod lisp-eval :number [[_ v] ctx] v)
(defmethod lisp-eval :nil [_ _] nil)
(defmethod lisp-eval :string [[_ v] ctx] v)
(defmethod lisp-eval :char [[_ v] ctx] v)
(defmethod lisp-eval :boolean [[_ v] ctx] v)
(defmethod lisp-eval :keyword [[_ v] ctx] v)
(defmethod lisp-eval :symbol [[_ v] ctx] (@ctx v))


(defmethod lisp-eval :quote [[_ v] ctx] v)

(defn make-function [args body fn-name ctx]
  [:compound args body fn-name ctx])

(declare actual-value)

(defn thunk? [expr]
  (and (vector? expr)
       (= (first expr) :thunk)))

(defn thunk-expr [[_ expr _]] expr)
(defn thunk-ctx  [[_ _ ctx]] ctx)

(defn delay-it [expr ctx]
  [:thunk expr ctx])

(defn force-it [x]
  (if (thunk? x)
    (actual-value (thunk-expr x) (thunk-ctx x))
    x))

(def actual-value
  (memoize
   (fn [expr ctx]
     (force-it (lisp-eval expr ctx)))))

(defn actual-values [xs ctx]
  (map #(actual-value % ctx) xs))

(defn delayed-exprs [xs ctx]
  (map #(delay-it % ctx) xs))

(defmethod lisp-eval :if [[_ {:keys [condition consequent alternative]}] ctx]
  (if (actual-value condition ctx)
    (lisp-eval consequent ctx)
    (lisp-eval alternative ctx)))

(defmethod lisp-eval :when [[_ {:keys [condition body]}] ctx]
  (lisp-eval [:if {:condition condition
                   :consequent [:do {:body body}]
                   :alternative [:nil nil]}]
             ctx))

(defmethod lisp-eval :do [[_ {:keys [body]}] ctx]
  (last (map #(lisp-eval % ctx) body)))

(defmethod lisp-eval :fn [[_ {:keys [args body name]}] ctx]
  (make-function args body name ctx))

(defmulti apply-function (fn [operand args ctx] (first operand)))

(defmethod apply-function :primitive [[_ f] args ctx]
  (let [values (actual-values args ctx)]
    (apply f values)))

(defmethod apply-function :core [[_ f] args ctx]
    (apply f ctx args))

(defn list->cons [x]
  (reduce (fn [a b]
            (->Cons b a))
          nil
          (reverse x)))

(defmethod apply-function :compound [[_ arg-names body fn-name fn-ctx :as f]
                                     arg-values ctx]
  (let [me (make-function arg-names body fn-name fn-ctx)
        args (delayed-exprs arg-values ctx)
        local-ctx (merge @fn-ctx (zipmap arg-names args)
                         {fn-name me
                          '-arguments (list->cons args)
                          'recur me})]
    (lisp-eval body (atom local-ctx))))

(defmethod lisp-eval :list [[_ [operand & args]] ctx]
  (apply-function (actual-value operand ctx)
                  args
                  ctx))


(defmethod lisp-eval :let [[_ {:keys [bindings body]}] ctx]
  (let [new-ctx (reduce (fn [ctx {:keys [symbol expr]}]
                          (assoc ctx symbol (delay-it expr (atom ctx))))
                        @ctx
                        bindings)]
    (lisp-eval [:do {:body body}] (atom new-ctx))))


(defmethod lisp-eval :def [[_ {:keys [name value]}] ctx]
  (swap! ctx assoc name (delay-it value (atom @ctx)))
  nil)

(defmethod lisp-eval :defn [[_ {:keys [name args body]}] ctx]
  (lisp-eval [:def {:name name,
                    :value [:fn {:args args,
                                 :name name
                                 :body [:do {:body body}]}]}]
             ctx))

