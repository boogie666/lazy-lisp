# lazy-lisp

A lazy toy lisp built in clojure with spec.

## Usage

`lein uberjar` - to compile the 'interpreter'

`java -jar ./target/lazy-lisp.jar ./resources/test_program.lclj` - to run the test program (a lazy fibonacci with some more functions added in for good measure)


# Primitive functions

There are only 12 primitive added currently

`+ - / * mod = < > print cons head tail`.


All primitives are first class and can be passed around as args cu user defined functions.

# Language 'features'

All language features a showcased in the test_program.lclj program, it's not much :)


## License

Copyright © 2018 boogie666

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
