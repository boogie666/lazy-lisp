(ns lazy-lisp.eval-test
  (:require [lazy-lisp.eval :as e]
            [clojure.test :refer :all]
            [lazy-lisp.syntax :as s]))

(defn new-ctx []
  (atom {'x "some_value"
         '+ [:primitive +]
         'print [:primitive println]
         'cons [:core e/cons]}))

(deftest eval-primitives
  (let [primitives [{:value 1 :type :number}
                    {:value "hello" :type :string}
                    {:value :hello :type :keyword}
                    {:value true :type :boolean}
                    {:value false :type :boolean}
                    {:value \a :type :char}
                    {:value nil :type :nil}]
        run-test (fn [{:keys [value type]}]
                   (is (= value (e/lisp-eval [type value] (new-ctx)))))]
    (testing "primitves"
      (doseq [p primitives]
        (run-test p)))))

(deftest eval-symbol
  (testing "looks its self up in context"
    (is (= "some_value" (e/lisp-eval [:symbol 'x] (new-ctx))))))

(deftest eval-do
  (testing "last value is returned"
    (is (= "hello") (e/lisp-eval (s/parse '(do "hello")) (new-ctx)))
    (is (= "hello") (e/lisp-eval (s/parse '(do 1 2 "hello")) (new-ctx)))))

(deftest eval-if
  (testing "when condition is true returns consequent branch"
    (is (= "consequent" (e/lisp-eval [:if {:condition [:boolean true]
                                           :consequent [:string "consequent"]
                                           :alternative [:string "alternative"]}]
                                     (new-ctx)))))
  (testing "when condition is false returns alternative branch"
    (is (= "alternative" (e/lisp-eval [:if {:condition [:boolean false]
                                            :consequent [:string "consequent"]
                                            :alternative [:string "alternative"]}]
                                      (new-ctx))))))

(deftest call-primitive-function
  (testing "calls a primitive with correct arguments"
    (is (= 3 (e/lisp-eval [:list [[:symbol '+] [:number 1] [:number 2]]]
                          (new-ctx))))))

(deftest call-core-functions
  (let [context (new-ctx)]
    (testing "calls a core function with correct anguments and context"
      (let [expected-result (e/cons context
                                    [:number 1]
                                    [:list [[:symbol 'cons] [:number 2] [:nil nil]]])
            actual-result (e/lisp-eval [:list [[:symbol 'cons]
                                               [:number 1]
                                               [:list [[:symbol 'cons] [:number 2] [:nil nil]]]]]
                                       context)]
        (is (= (:head expected-result)
               (:head actual-result)))
        (is (= (:tail expected-result)
               (:tail actual-result)))))))

(deftest eval-fn
  (let [context (new-ctx)]
    (testing "Eval a user defined function"
      (let [user-defined-function (e/lisp-eval [:fn {:name 'f
                                                     :args [[:symbol 'x]]
                                                     :body [[:symbol 'x]]}]
                                               context)
            [type args body name ctx] user-defined-function]
        (is (= type :compound))
        (is (= args [[:symbol 'x]]))
        (is (= body [[:symbol 'x]]))
        (is (= ctx context))))))

(deftest call-user-defined-function
  (let [context (new-ctx)
        f [:list [[:fn {:name 'f,
                        :args ['x],
                        :body [:symbol 'x]}]
                  [:number 1]]]
        expected-result [:thunk [:number 1] context]]
    (testing "call to a userdefined function results in a thunk"
      (is (= expected-result (e/lisp-eval f context))))
    (testing "forceing the result yeads the value"
      (is (= 1 (e/force-it (e/lisp-eval f context)))))))

(deftest forceing-cons
  (let [context (new-ctx)]
    (testing "can eval head on cons"
      (is (= 1 (e/force-it (e/head (e/cons context [:number 1] [:nil nil]))))))
    (testing "can eval tail on cons"
      (is (= nil (e/force-it (e/tail (e/cons context [:number 1] [:nil nil]))))))
    (testing "can eval head expr"
      (is (= "some_value" (e/force-it (e/head (e/cons context [:symbol 'x] [:nil nil]))))))))

(deftest simple-eval
  (let [context (new-ctx)
        program '(let [inc (fn [x] (+ x 1))]
                   (inc 1))]
    (is (= 2
           (-> program
               (s/parse)
               (e/lisp-eval context))))))

(deftest def-eval
  (let [context (new-ctx)
        program '(def q 1)]

    (-> program
        (s/parse)
        (e/lisp-eval context))
    (testing "def adds a new value on context"
      (is (= 1 (e/force-it (@context 'q)))))))

